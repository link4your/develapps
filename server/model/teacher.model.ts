export interface teacher {
    _id: string,
    nombre: string,
    apellidos: string,
    direccion: string,
    telefono: string,
    imagen: any
}
