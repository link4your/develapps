import { Router, Response, Request } from "express";
import { MongoError, ObjectId } from "mongodb";
import { teacher } from "../model/teacher.model";
const auth = require('../middlewares/authToken');
const helper = require('../middlewares/helpers');


const routes = Router();
const multer = require('multer')
const path = require('path');


const storage = multer.diskStorage({
    destination: function (req: any, file: any, cb: any) {
        cb(null, path.join(__dirname, '../../public'));
    },
    filename: function (req: any, file: any, cb: any) {
        const mime = file.mimetype.split('/');
        const mimetype = mime[1];
        cb(null, file.fieldname + '-' + Date.now()+ '.' + mimetype)
    }
});


const upload = multer({
    limits: {
        fileSize: 4 * 1024 * 1024
    },

    storage: storage

})



routes.put('/upload', upload.single('imagen'), (req: any, res: Response) => {
    const db = req.app.locals.db;
    const _id = new ObjectId(req.body._id);
    console.log('aqui estoy')
    console.log(req.file.name);
  
    db.collection('teachers').findOneAndUpdate({ _id: _id }, { $set: { imagen: req.file.filename } }).then((val: any) => {
        return res.status(200).send(true)
    }).catch((err: MongoError) => {
        console.log(err);
        res.status(500).send(false);
    })
});

routes.put('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const body = req.body;
    const _id = new ObjectId(body._id);
    delete body['_id'];

    helper.validateDataForm(['nombre', 'apellidos,', 'direccion', 'telefono'], body).then((val: any) => {
        if (!val) {
            return res.status(200).send(false);
        } else {

            db.collection('teachers').findOneAndUpdate({ _id: _id }, { $set: body }).then((value: any) => {
                return res.status(200).send(true);
            }).catch((err: MongoError) => {
                console.log(err);
                return res.status(400).send(false);
            });

        }
    });



});


routes.post('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const body: teacher = req.body;
    delete body['_id'];

    helper.validateDataForm(['nombre', 'apellidos,', 'direccion', 'telefono', 'imagen'], body).then((val: any) => {
        if (!val) {
            return res.status(200).send(false);
        } else {
          
            db.collection('teachers').insertOne(body).then((val: any) => {
                return res.status(200).send(val.insertedId);
            }).catch((err: MongoError) => {
                return res.status(400).send(false);
            });

        }
    });


});


routes.get('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    db.collection('teachers').find().toArray().then((value: any) => {
        return res.status(200).send(value);
    }).catch((err: MongoError) => {
        return res.status(400).send(false);
    });
});


routes.get('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const _id = new ObjectId(req.params.id);
    db.collection('teachers').findOne({ _id: _id }).then((value: any) => {
        return res.status(200).send(value);
    }).catch((err: MongoError) => {
        return res.status(400).send(false);
    });
});


routes.delete('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const _id = new ObjectId(req.params.id);
    db.collection('teachers').deleteOne({ _id: _id }).then((value: any) => {
        return res.status(200).send(value);
    }).catch((err: MongoError) => {
        return res.status(400).send(false);
    });
});






export default routes;
