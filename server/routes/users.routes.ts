import {Router, Response, Request} from "express";
import {MongoError, ObjectId} from "mongodb";
import bcrypt from 'bcryptjs';
import Token from "../classes/token";
const  multer  = require('multer');
const  upload = multer({ dest: 'uploads/' });
const routes = Router();


routes.get('/auth/:id', (req: Request, res: Response) => {
    const token = req.params.id;
    Token.comprobarToken(token).then(() => {
        return res.status(200).json({token:token});
    }).catch(() => {
        return res.status(200).send(false);
    });

});

routes.post('/login', (req: Request, res: Response) => {
    const db = req.app.locals.db;
    const user: any = req.body;
    db.collection('users').findOne({'email': user.email}).then((value: any) => {
        if (!value)
            return res.status(200).send(false);
        if (value.token)
            return res.status(200).send(false);

        bcrypt.compare(user.password, value.password, (err, resp) => {
            if (!resp)
                return res.status(200).send(false);

            const tokenUser = Token.getJwtToken(value);
            return res.status(200).json({token: tokenUser});
        });
    }).catch((err: MongoError) => {
        return res.status(401).send(false);
    })
});





export default routes;
