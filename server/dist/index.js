"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./classes/server"));
const express = require('express');
const users_routes_1 = __importDefault(require("./routes/users.routes"));
const student_routes_1 = __importDefault(require("./routes/student.routes"));
const teacher_routes_1 = __importDefault(require("./routes/teacher.routes"));
const bodyParser = require('body-parser');
const cors = require('cors');
const server = new server_1.default();
const path = require('path');
// Bodyparser
server.app.use(bodyParser.urlencoded({ extended: true }));
server.app.use(bodyParser.json());
// CORS
server.app.use(cors({ origin: true, credentials: true }));
server.app.use(express.static("public"));
//Rutas
server.app.use('/user', users_routes_1.default);
server.app.use('/student', student_routes_1.default);
server.app.use('/teacher', teacher_routes_1.default);
// LEVANTAR SERVER
server.start(() => {
    console.log(`Servidor corriendo en el puerto ${server.port}`);
});
