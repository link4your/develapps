"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const enviroments_1 = require("../enviroments/enviroments");
const MongoClient = require('mongodb').MongoClient;
class Server {
    constructor() {
        this.port = enviroments_1.SERVER_PORT;
        this.app = express_1.default();
    }
    start(callback) {
        MongoClient.connect(enviroments_1.DB_URL, { useNewUrlParser: true }, (err, db) => {
            if (err) {
                console.log(`Failed to connect to the database. ${err.stack}`);
            }
            this.app.locals.db = db.db('develapps');
            this.app.listen(this.port, callback);
        });
    }
}
exports.default = Server;
