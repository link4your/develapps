"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Extensions {
    constructor() {
        this.mimeType = {
            "images/jpeg": "jpg",
            "images/gif": "gif",
            "images/bmp": "bmp",
            "images/tiff": "tiff",
            "images/pn": "png"
        };
    }
    getExtension(_mimeType) {
        if (_mimeType || typeof _mimeType !== undefined) {
            return this.mimeType[_mimeType] ? _mimeType[this.mimeType] : console.log('mime no encontrado');
        }
    }
}
exports.Extensions = Extensions;
