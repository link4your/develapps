"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SERVER_PORT = Number(process.env.PORT) || 8000;
exports.SECRET = 'cvy6gv~€¬~frde6##@';
let dbAuth = '';
if (process.env.DBPASSWORD && process.env.DBUSER) {
    dbAuth = `${process.env.DBPASSWORD}:${process.env.DBUSER}@`;
}
exports.DB_URL = `mongodb://${dbAuth}localhost:27017`;
