"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongodb_1 = require("mongodb");
const helper = require('../middlewares/helpers');
const auth = require('../middlewares/authToken');
const routes = express_1.Router();
const multer = require('multer');
const path = require('path');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../../public'));
    },
    filename: function (req, file, cb) {
        const mime = file.mimetype.split('/');
        const mimetype = mime[1];
        cb(null, file.fieldname + '-' + Date.now() + '.' + mimetype);
    }
});
const upload = multer({
    limits: {
        fileSize: 4 * 1024 * 1024
    },
    storage: storage
});
routes.put('/upload', upload.single('imagen'), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.body._id);
    db.collection('students').findOneAndUpdate({ _id: _id }, { $set: { imagen: req.file.filename } }).then((val) => {
        return res.status(200).send(req.file.filename);
    }).catch((err) => {
        console.log(err);
        res.status(500).send(false);
    });
});
routes.post('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const body = req.body;
    helper.validateDataForm(['nombre', 'apellidos,', 'direccion', 'telefono', '_id', 'imagen'], body).then((val) => {
        if (!val) {
            return res.status(200).send(false);
        }
        else {
            db.collection('students').insertOne(body).then((val) => {
                return res.status(200).send(val.insertedId);
            }).catch((err) => {
                console.log(err);
                return res.status(500).send(false);
            });
        }
    }).catch((err) => {
        return res.status(500).send(false);
    });
});
routes.get('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    db.collection('students').find().toArray().then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.get('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('students').findOne({ _id: _id }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.delete('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('students').deleteOne({ _id: _id }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.put('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.body._id);
    let body = req.body;
    delete body['_id'];
    helper.validateDataForm(['nombre', 'apellidos,', 'direccion', 'telefono', 'imagen'], body).then((val) => {
        if (!val) {
            return res.status(200).send(false);
        }
        else {
            db.collection('students').findOneAndUpdate({ _id: _id }, { $set: body }).then((val) => {
                return res.status(200).send(val);
            }).catch((err) => {
                console.log(err);
                return res.status(500).send(false);
            });
        }
    }).catch((err) => {
        return res.status(500).send(false);
    });
});
routes.get('/free/students', (req, res) => {
    const db = req.app.locals.db;
    db.collection('students').aggregate([{ $match: { 'teacher': { $exists: false } } }]).toArray().then((val) => {
        return res.status(200).send(val);
    }).catch((err) => { console.log(err); return res.status(500).send(false); });
});
routes.get('/taken/students/:id', (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('students').find({ 'teacher': _id }).toArray().then((val) => {
        return res.status(200).send(val);
    }).catch((err) => { console.log(err); return res.status(500).send(false); });
});
routes.put('/add/students/teacher', (req, res) => {
    const db = req.app.locals.db;
    const studentId = new mongodb_1.ObjectId(req.body.studentId);
    if (req.body.teacherId) {
        const teacherId = new mongodb_1.ObjectId(req.body.teacherId);
        db.collection('students').findOneAndUpdate({ _id: studentId }, { $set: { 'teacher': teacherId } }).then((val) => {
            return res.status(200).send(val.value);
        }).catch((err) => { console.log(err); return res.status(500).send(false); });
    }
    else {
        return res.status(200).send(false);
    }
});
routes.delete('/delete/students/teacher/:id', (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('students').findOneAndUpdate({ _id: _id }, { $unset: { 'teacher': '' } }).then((val) => {
        return res.status(200).send(val.value);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
exports.default = routes;
