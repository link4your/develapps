"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongodb_1 = require("mongodb");
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const auth = require('../middlewares/authToken');
const routes = express_1.Router();
routes.put('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR', 'ROLE_USER']), (req, res) => {
    const db = req.app.locals.db;
    const body = req.body;
    const _id = new mongodb_1.ObjectId(body._id);
    delete body['_id'];
    db.collection('crimes').findOneAndUpdate({ _id: _id }, { $set: body }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        console.log(err);
        return res.status(400).send(false);
    });
});
routes.post('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR', 'ROLE_USER']), (req, res) => {
    const db = req.app.locals.db;
    const body = req.body;
    delete body['_id'];
    body.user = new mongodb_1.ObjectId(body.user);
    db.collection('crimes').insertOne(body).then((value) => {
        formatedData(body).then((values) => {
            if (values) {
                db.collection('crimeTables').insertOne(values).then((val) => {
                    console.log(val);
                    return res.status(200).send(val.insertedId);
                }).catch((err) => {
                    console.log(err);
                    return res.status(400).send(false);
                });
            }
        }).catch((err) => {
            console.log(err);
            return res.status(400).send(false);
        });
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.get('/', auth.checkAuthorization(['ROLE_ADMINISTRATOR', 'ROLE_USER']), (req, res) => {
    const db = req.app.locals.db;
    db.collection('crimes').find().toArray().then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.get('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR', 'ROLE_USER']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('crimes').findOne({ _id: _id }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.delete('/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('crimes').deleteOne({ _id: _id }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.get('/pending', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    db.collection('pending').find().toArray().then((val) => {
        if (val) {
            return res.status(200).send(val);
        }
        else {
            res.status(200).send(false);
        }
    }).catch((err) => {
        return res.status(401).send(false);
    });
});
const formatedData = (data) => {
    return new Promise((resolve, reject) => {
        let fomated = {
            criminal: [],
            victim: []
        };
        // @ts-ignore
        fomated.date = data.crimeDate;
        // @ts-ignore
        fomated.type = data.crimeType;
        data.criminal.forEach((item) => {
            let criminal = {};
            criminal.age = item.age;
            criminal.sex = item.sex;
            criminal.race = item.race;
            criminal.country = item.country;
            fomated.criminal.push(criminal);
        });
        data.victim.forEach((item) => {
            let victim = {};
            victim.age = item.age;
            victim.sex = item.sex;
            victim.race = item.race;
            victim.country = item.country;
            fomated.victim.push(victim);
        });
        resolve(fomated);
    });
};
exports.default = routes;
