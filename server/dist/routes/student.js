"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongodb_1 = require("mongodb");
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const auth = require('../middlewares/authToken');
const routes = express_1.Router();
// GET COUNTRIES
routes.get('/countries', (req, res) => {
    const db = req.app.locals.db;
    db.collection('countries').find().toArray().then((values) => {
        return res.status(200).send(values);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
// POST PUT GET CRIMETYPES
routes.post('/crime', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const body = req.body;
    db.collection('crimeTypes').insertOne({ 'name': body.name }).then(() => {
        return res.status(200).send(true);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.get('/crime', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    db.collection('crimeTypes').find().toArray().then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.get('/crime/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('crimeTypes').findOne({ _id: _id }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.delete('/crime/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('crimeTypes').deleteOne({ _id: _id }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.put('/crime', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.body._id);
    const body = req.body;
    db.collection('crimeTypes').findOneAndUpdate({ _id: _id }, { $set: { 'name': body.name } }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
// POST PUT GET DELETE RACE
routes.post('/race', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const body = req.body;
    db.collection('race').insertOne({ 'name': body.name }).then(() => {
        return res.status(200).send(true);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.get('/race', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    db.collection('race').find().toArray().then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.get('/race/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('race').findOne({ _id: _id }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.delete('/race/:id', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('race').deleteOne({ _id: _id }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
routes.put('/race', auth.checkAuthorization(['ROLE_ADMINISTRATOR']), (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.body._id);
    const body = req.body;
    db.collection('race').findOneAndUpdate({ _id: _id }, { $set: { 'name': body.name } }).then((val) => {
        return res.status(200).send(val);
    }).catch((err) => {
        console.log(err);
        return res.status(500).send(false);
    });
});
exports.default = routes;
