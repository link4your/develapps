"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mongodb_1 = require("mongodb");
const routes = express_1.Router();
routes.get('/', (req, res) => {
    const db = req.app.locals.db;
    db.collection('crimes').find().toArray().then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.get('/pending', (req, res) => {
    const db = req.app.locals.db;
    db.collection('pending').find().toArray().then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.get('/:id', (req, res) => {
    const db = req.app.locals.db;
    const _id = new mongodb_1.ObjectId(req.params.id);
    db.collection('crimes').findOne({ _id: _id }).then((value) => {
        return res.status(200).send(value);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
routes.post('/', (req, res) => {
    const db = req.app.locals.db;
    const body = checkParams(req.body);
    delete body['_id'];
    body.user = new mongodb_1.ObjectId(body.user);
    db.collection('pending').insertOne(body).then((value) => {
        console.log(value);
        return res.status(200).send(true);
    }).catch((err) => {
        return res.status(400).send(false);
    });
});
const checkParams = (body) => {
    const params = ['title', 'psinopsis', 'crimeType', 'crimeDate'];
    const keys = Object.keys(body);
    keys.forEach(key => {
        params.forEach(param => {
            if (key !== param) {
                body[param] = null;
            }
        });
    });
    return body;
};
exports.default = routes;
