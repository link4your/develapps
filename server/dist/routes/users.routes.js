"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const token_1 = __importDefault(require("../classes/token"));
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const routes = express_1.Router();
routes.get('/auth/:id', (req, res) => {
    const token = req.params.id;
    token_1.default.comprobarToken(token).then(() => {
        return res.status(200).json({ token: token });
    }).catch(() => {
        return res.status(200).send(false);
    });
});
routes.post('/login', (req, res) => {
    const db = req.app.locals.db;
    const user = req.body;
    db.collection('users').findOne({ 'email': user.email }).then((value) => {
        if (!value)
            return res.status(200).send(false);
        if (value.token)
            return res.status(200).send(false);
        bcryptjs_1.default.compare(user.password, value.password, (err, resp) => {
            if (!resp)
                return res.status(200).send(false);
            const tokenUser = token_1.default.getJwtToken(value);
            return res.status(200).json({ token: tokenUser });
        });
    }).catch((err) => {
        return res.status(401).send(false);
    });
});
exports.default = routes;
