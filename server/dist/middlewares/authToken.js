"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require('jsonwebtoken');
const SEED = require('../enviroments/enviroments').SECRET;
exports.checkAuthorization = function (rolesAuthorized) {
    return function (req, res, next) {
        const token = jwt.decode(req.headers.authorization);
        req.headers.authorization = token;
        if (token === undefined || token === null) {
            // Si no se ha enviado JWT o está vacío.
            res.status(400).send(false);
            console.log('[MIDDLEWARE] : Empty JWT');
        }
        else {
            let rolesUsed = token.user.role;
            let coincidence = rolesUsed.some((roleUsed) => rolesAuthorized.indexOf(roleUsed) >= 0);
            // Si se ha enviado y tiene permisos
            if (coincidence)
                next();
            else {
                // Si se ha enviado pero no tiene permisos
                res.status(400).send(false);
                console.log('[MIDDLEWARE] : Unauthorized');
                console.log('[MIDDLEWARE] : ROLE(S) used', token.role);
                console.log('[MIDDLEWARE] : ROLE(S) auth', rolesAuthorized);
            }
        }
    };
};
