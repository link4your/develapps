"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateDataForm = (arrayData, body) => {
    console.log(arrayData);
    console.log(body);
    const countData = arrayData.length;
    const keys = Object.keys(body);
    let counter = 1;
    return new Promise((resolve, reject) => {
        if (keys.length < countData) {
            resolve(false);
        }
        else {
            const keys = Object.keys(body);
            keys.forEach(key => {
                if (body[key] === '' && body[key] === null && body[key] === undefined) {
                    resolve(false);
                }
                else {
                    arrayData.forEach((arr) => {
                        if (arr == key) {
                            counter++;
                        }
                    });
                }
            });
            if (counter === countData) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        }
    });
};
