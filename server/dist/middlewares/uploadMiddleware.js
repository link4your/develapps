"use strict";
const multer = require('multer');
const path = require('path');
const extension = require('../classes/mimeTypes');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join('../', 'public/images'));
    },
    filename: function (req, file, cb) {
        const ext = new extension();
        cb(null, file.fieldname + '-' + Date.now() + ext.getExtension(file.mimetype));
    }
});
exports.upload = () => {
    return multer({
        limits: {
            fileSize: 4 * 1024 * 1024
        },
        dest: path.join(__dirname, '../', 'public/images'),
        storage: storage
    });
};
