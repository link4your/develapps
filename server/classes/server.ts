import express from 'express';
import {DB_URL, SERVER_PORT} from '../enviroments/enviroments';
const MongoClient = require('mongodb').MongoClient;
export default class Server {
    public app: express.Application;
    public port: number = SERVER_PORT;
 constructor(){
     this.app = express();
 }

 start(callback: Function){
     MongoClient.connect(DB_URL,  {useNewUrlParser: true}, (err: any, db: any) => {
         if (err) {
             console.log(`Failed to connect to the database. ${err.stack}`);
         }
         this.app.locals.db = db.db('develapps');
         this.app.listen( this.port ,callback );
     });
 }
}
