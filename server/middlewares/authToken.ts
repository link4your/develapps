const jwt = require('jsonwebtoken');
const SEED = require('../enviroments/enviroments').SECRET;
import {Request, Response} from 'express';



exports.checkAuthorization = function (rolesAuthorized:any) {

    return function(req: Request, res:Response, next:any) {

        const token = jwt.decode(req.headers.authorization);
        req.headers.authorization = token;

       

        if (token === undefined || token === null){
            // Si no se ha enviado JWT o está vacío.
            res.status(400).send(false);
            console.log('[MIDDLEWARE] : Empty JWT');
        } else {
            let rolesUsed = token.user.role;

            let coincidence = rolesUsed.some((roleUsed:any) => rolesAuthorized.indexOf(roleUsed) >= 0)
           
            // Si se ha enviado y tiene permisos
            if (coincidence) next();
            else {
                // Si se ha enviado pero no tiene permisos
                res.status(400).send(false);
                console.log('[MIDDLEWARE] : Unauthorized');
                console.log('[MIDDLEWARE] : ROLE(S) used', token.role);
                console.log('[MIDDLEWARE] : ROLE(S) auth', rolesAuthorized);
            }
        }
    }
}