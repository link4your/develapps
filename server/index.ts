import Server from "./classes/server";
const express = require('express'); 
import userRouter from './routes/users.routes';
import studentRouter from './routes/student.routes';
import teacherRouter from './routes/teacher.routes';
const bodyParser = require('body-parser');
const cors = require('cors');
const server = new Server();
const path = require('path')


// Bodyparser
server.app.use(bodyParser.urlencoded({extended: true}));
server.app.use(bodyParser.json());
// CORS
server.app.use(cors({origin: true, credentials: true}));

server.app.use(express.static("public"));



//Rutas
server.app.use('/user',userRouter);
server.app.use('/student', studentRouter);
server.app.use('/teacher', teacherRouter);


// LEVANTAR SERVER
server.start(()=>{
    console.log(`Servidor corriendo en el puerto ${server.port}`);
});
