
import { DashboardComponent } from '../app/dashboard/dashboard/dashboard.component';
import { dashboardRoutesModule} from '../app/dashboard/dashboard/dashboard.routes.module';
import { AuthGuardService } from '../app/auth/auth-guard.service';
import { LoginComponent } from '../app/auth/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  
   // {path: '', component: DashboardComponent, canActivate: [AuthGuardService], children: dashboardRoutesModule},
   {path: '', component: DashboardComponent, canActivate: [AuthGuardService], children: dashboardRoutesModule},
    {path: 'login', component: LoginComponent} 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
