import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from '../shared/sidebar/sidebar.component';
import { FooterComponent } from '../shared/footer/footer.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { AppRoutingModule } from '../app-routing.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule, MatListModule } from '@angular/material';




@NgModule({
  declarations: [
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    NavbarComponent
   ],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule
  ],
  exports: [
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    NavbarComponent
  ]
})
export class SharedModule { }
