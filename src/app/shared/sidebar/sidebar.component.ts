import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
 menu: any[] = [
   {menu : 'Profesores' , 'url': 'teacher/list' }, {menu: 'Alumnos', 'url': 'student/list'}];
  constructor() { }

  ngOnInit() {
  }

}
