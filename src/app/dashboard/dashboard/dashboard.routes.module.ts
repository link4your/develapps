
import { ConfigComponent } from '../../components/config/config.component';

import { AddTeacherComponent } from '../../components/add-teacher/add-teacher.component';
import { ListTeacherComponent } from '../../components/list-teacher/list-teacher.component';
import { ListStudentComponent } from '../../components/list-student/list-student.component';
import { AddStudentComponent } from '../../components/add-student/add-student.component';




import { Routes } from '@angular/router';



export const dashboardRoutesModule: Routes = [
  
      { path: 'teacher/add', component: AddTeacherComponent },
      { path: 'teacher/add/:id', component: AddTeacherComponent },
      { path: 'teacher/list', component: ListTeacherComponent },
      { path: 'student/add', component: AddStudentComponent },
      { path: 'student/add/:id', component: AddStudentComponent },
      { path: 'student/list', component: ListStudentComponent },
      {path: 'config', component: ConfigComponent },
      {path: 'config/:id', component: ConfigComponent},
      { path: '', redirectTo: 'teacher/list', pathMatch: 'full' }
  



];
