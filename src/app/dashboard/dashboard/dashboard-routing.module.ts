import {NgModule} from '@angular/core';
import {DashboardComponent} from './dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {dashboardRoutesModule} from './dashboard.routes.module';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: dashboardRoutesModule
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class DashboardRoutingModule {
}
