import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private  _auth: AuthService) {}
  canActivate() {
    
    return this.permission().then(item => {
      if (item) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      
      }
    });
  }

  permission(): Promise<boolean> {
    const token = localStorage.getItem('t_c_a');
    return new Promise((observer) => {
      fetch(environment.url + 'user/auth/' + token).then(resp => resp.json()).then(respObj => {
        observer(respObj);
      }).catch(() => observer(false));
    });
  }
}

