import { Component, OnInit, ViewChild } from '@angular/core';
import {environment } from '../../../environments/environment';
import { ComponentsService } from '../components.service';
import { teacher } from '../add-teacher/add-teacher.component';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-teacher',
  templateUrl: './list-teacher.component.html'

})
export class ListTeacherComponent {
  teachers: any;
  displayedColumns: string[] = ['imagen','nombre', 'apellidos', 'telefono', 'acciones', 'add'];
  url = environment.url;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private _component: ComponentsService) {
    this._component.getTeachers().subscribe((response: teacher[]) => {
      this.teachers = new MatTableDataSource<teacher>(response);
      this.teachers.paginator = this.paginator;
      this.teachers.sort = this.sort;
    });
  }


  delete(_id: string) {

    if (_id) {

      this._component.deleteTeacher(_id).subscribe(response => {
         
        const eliminado = this.teachers.data.filter(item => item._id === _id);
     
        this.teachers.data = this.teachers.data.filter(item => item._id !== _id);
        if (response) {
           Swal.fire('Hecho', `Has eliminado a ${eliminado[0].nombre}  ${eliminado[0].apellidos}`, 'success');
        } else {
          Swal.fire('Error', `Al elimiar  a ${eliminado[0].nombre}  ${eliminado[0].apellidos}`, 'warning');
        }
      });

    }
  }


}
