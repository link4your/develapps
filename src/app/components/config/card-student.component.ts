import { environment } from '../../../environments/environment';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { ComponentsService } from '../components.service';
import { student, Student } from '../add-student/add-student.component';


@Component({
  selector: 'app-card-student',
  templateUrl: './card-student.component.html'
})
export class CardStudentComponent implements OnInit {
  @Input() student: Student = new Student();
  @Input() taken: boolean;
  @Input() _ref: any;
  url = environment.url;
  
  @Output() deleteEmitter: EventEmitter<any> = new EventEmitter<any>();
  
  constructor(private _component: ComponentsService){}

  ngOnInit() {
  }

  delete(_id: string){
    this._ref.destroy();
    this._component.deleteStudentTeacher(_id).subscribe(response => {
     
     if(response) {
      
       this.deleteEmitter.emit(_id);
     } else {
      this.deleteEmitter.emit(false);
     }
    });
  }

}
