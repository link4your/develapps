import { Component, OnInit, ComponentFactoryResolver, ComponentRef, HostListener, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentsService } from '../components.service';
import { student, Student } from '../add-student/add-student.component';
import { Teacher} from '../add-teacher/add-teacher.component';
import { CardStudentComponent } from './card-student.component';

import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html'
})
export class ConfigComponent implements OnInit {
  students: Student[] = [];
  teacherId: string;
  teachers: Teacher[] = [];
  teacher: Teacher = new Teacher;
  
 

  @ViewChild('canvasA', { read: ViewContainerRef, static: true }) canvasA: ViewContainerRef;
  @ViewChild('canvasB', { read: ViewContainerRef, static: true }) canvasB: ViewContainerRef;
  public cardStudent: ComponentRef<CardStudentComponent> = null;
  @HostListener('dragover', ['$event'])
  onDragOver(event: DragEvent) {
    event.stopPropagation();
    event.preventDefault();
  }

  // Al dejar de un elemento
  @HostListener('dragleave', ['$event'])
  onDragLeave(event: DragEvent) {
  }

  // Cuando empezamos a arrastrar
  @HostListener('dragstart', ['$event'])
  onDragStart(event: any) {
  
    event.dataTransfer.setData('text/plain', JSON.stringify(event.target.id));
  }

  // Cuando soltamos encima de algun elemento
  @HostListener('drop', ['$event'])
  onDrop(event: any) {
    

    let paramId = JSON.parse(event.dataTransfer.getData('text/plain'));
    event.preventDefault();

    if(paramId) {
        this._component.addStudentToTeacher({ 'studentId': paramId, 'teacherId':this.teacherId}).subscribe((response: Student) => {
        if(response) {
          const factory = this.resolver.resolveComponentFactory(CardStudentComponent);
          this.cardStudent = this.canvasB.createComponent(factory);
          this.cardStudent.instance._ref = this.cardStudent;
          this.cardStudent.instance.student.nombre = response.nombre;
          this.cardStudent.instance.student.apellidos = response.apellidos;
          this.cardStudent.instance.student._id = response._id;
          this.cardStudent.instance.student.imagen = response.imagen;
          this.cardStudent.instance.taken = true;
          this.cardStudent.instance.deleteEmitter.subscribe(response => {
            document.getElementById(response).hidden = false;
          })
          document.getElementById(paramId).hidden = true;
        } else {
         Swal.fire('No se pudo agregar', 'El alumno no esta asociado a ningun maestro', 'warning');
        }
       });

    }
 }




  constructor(private _component: ComponentsService, private resolver: ComponentFactoryResolver, private route: ActivatedRoute ) {

    this.freeStudents();


  

  this.route.params.subscribe(param => {
   if(param.id) {
    this._component.getTakenStudents(param.id).subscribe((response: Student[]) => {
     
      this.students = response;
      this.teacherId = param.id;
      this._component.getTeacher(this.teacherId).subscribe((response: Teacher) => this.teacher = response);
  
      const factory = this.resolver.resolveComponentFactory(CardStudentComponent);
      this.students.forEach(student => {
        this.cardStudent = this.canvasB.createComponent(factory);
        this.cardStudent.instance._ref = this.cardStudent;
        this.cardStudent.instance.student.nombre = student.nombre;
        this.cardStudent.instance.student.apellidos = student.apellidos;
        this.cardStudent.instance.student._id = student._id;
        this.cardStudent.instance.taken = true;
        this.cardStudent.instance.student.imagen = student.imagen;

        this.cardStudent.instance.deleteEmitter.subscribe( response => {
        document.getElementById(response).hidden = false
       });
      });
    });
   } else {
     this._component.getTeachers().subscribe((response: Teacher[]) => this.teachers = response)
   }

  });
   

  }


 chargeTeacher(){
   const te  = this.teachers.filter(item => item._id == this.teacherId);
   this.teacher = te[0];
 }


  freeStudents(){
    this._component.getFreeStudents().subscribe((response: Student[]) => {
    //  this.canvasA.clear();
      this.students = response;
    
      const factory = this.resolver.resolveComponentFactory(CardStudentComponent);
      this.students.forEach(student => {
        this.cardStudent = this.canvasA.createComponent(factory);
        this.cardStudent.instance._ref = this.cardStudent;
        this.cardStudent.instance.student.nombre = student.nombre;
        this.cardStudent.instance.student.apellidos = student.apellidos;
        this.cardStudent.instance.student._id = student._id;
        this.cardStudent.instance.student.imagen = student.imagen;

        this.cardStudent.instance.deleteEmitter.subscribe( response => {
         this._component.getFreeStudents().subscribe(response => {

         });
      });

      });
    });
  }



  

  ngOnInit() {
  }





}
