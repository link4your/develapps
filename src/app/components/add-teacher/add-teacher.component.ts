
import { ComponentsService} from '../components.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import {  SubirArchivoService  } from '../subir-archivo.service';

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html'
 
})
export class AddTeacherComponent  {
  teacher: Teacher = new Teacher();
  fileData: File = null;
  constructor(private _component: ComponentsService,private _router: Router, private _route: ActivatedRoute, private _upload: SubirArchivoService  ) {
    this._route.params.subscribe(params => {
      if(params.id){
        this._component.getTeacher(params.id).subscribe( (response: Teacher) => this.teacher = response);
      }
    });
   }


   uploadImage(fileInput: any){
    
    this.fileData = <File>fileInput;
  }
  

onSubmit(f: NgForm) {
if(f.valid) {
  if(this.teacher._id) {
    this._component.updateTeacher(this.teacher).subscribe( response => {

      if(response) {
        Swal.fire('Hecho', 'El profesor se actualizo con exito', 'success');
        this._router.navigate(['/teacher/list']);
     } else {
       Swal.fire('Atencion', 'Algo ha debido fallar', 'warning');
     }
    });
   }
   else {
    const teacher = new Teacher(f.value);
    this._component.addTeacher(teacher).subscribe( response => {
      if(this.fileData) {
        this._upload.subirArchivo(this.fileData, response, 'teacher').then(()=> {
     if(response) {
         Swal.fire('Hecho', 'El profesor se agrego con exito', 'success');
         this._router.navigate(['/teacher/list']);
      } else {
        Swal.fire('Atencion', 'Algo ha debido fallar', 'warning');
      }
        });
      } else {
        if(response) {
          Swal.fire('Hecho', 'El profesor se agrego con exito', 'success');
          this._router.navigate(['/teacher/list']);
       } else {
         Swal.fire('Atencion', 'Algo ha debido fallar', 'warning');
       }

      }
    
     
    })

   };
}

}
}

export interface teacher {
  _id?: string;
  nombre: string,
  apellidos: string,
  direccion: string,
  telefono: number,
  imagen: any
}

export class Teacher {
  _id: string;
  nombre: string;
  apellidos: string;
  direccion: string;
  telefono: number;
  imagen: any;

  constructor(data?: teacher) {
    this._id = data && data._id || null;
    this.nombre = data && data.nombre || null;
    this.apellidos = data && data.apellidos || null;
    this.direccion = data && data.direccion || null;
    this.telefono = data && data.telefono || null;
    this.imagen = data && data.imagen || null;
   
  }
}