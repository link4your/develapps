import {environment } from '../../../environments/environment';
 import { student } from '../add-student/add-student.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentsService } from '../components.service';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html'
})
export class ListStudentComponent  {

  students: any;
  displayedColumns: string[] = ['imagen', 'nombre', 'apellidos', 'telefono', 'acciones', 'add'];
  url = environment.url;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private _component: ComponentsService) {
    this._component.getStudents().subscribe((response: student[]) => {
      this.students = new MatTableDataSource<student>(response);
      this.students.paginator = this.paginator;
      this.students.sort = this.sort;
    });
  }


  delete(_id: string) {

    if (_id) {

      this._component.deleteStudent(_id).subscribe(response => {
         
        const eliminado = this.students.data.filter(item => item._id === _id);
     
        this.students.data = this.students.data.filter(item => item._id !== _id);
        if (response) {
           Swal.fire('Hecho', `Has eliminado a ${eliminado[0].nombre}  ${eliminado[0].apellidos}`, 'success');
        } else {
          Swal.fire('Error', `Al elimiar  a ${eliminado[0].nombre}  ${eliminado[0].apellidos}`, 'warning');
        }
      });

    }
  }

}
