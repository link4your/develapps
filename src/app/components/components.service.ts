import { Injectable } from '@angular/core';

import { Teacher } from './add-teacher/add-teacher.component';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Student } from './add-student/add-student.component';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  constructor(private http: HttpClient) { }
  addTeacher(teacher: Teacher){
    return this.http.post(environment.url + 'teacher' , teacher);
  }

  updateTeacher(teacher: Teacher){
    return this.http.put(environment.url + 'teacher', teacher);
  }

  getTeachers(){
    return this.http.get(environment.url + 'teacher');
  }

  getTeacher(_id: string){
    return this.http.get(environment.url + 'teacher/' + _id);
  }

  deleteTeacher(_id: string) {
    return this.http.delete(environment.url + 'teacher/' + _id);
  }


  addStudent(student: Student){
    return this.http.post(environment.url + 'student' , student);
  }

  updateStudent(student: Student){
    return this.http.put(environment.url + 'student', student);
  }

  getStudents(){
    return this.http.get(environment.url + 'student');
  }

  getStudent(_id: string){
    return this.http.get(environment.url + 'student/' + _id);
  }

  deleteStudent(_id: string) {
    return this.http.delete(environment.url + 'student/' + _id);
  }


  getFreeStudents(){
    return this.http.get(environment.url + 'student/free/students');
  }

  getTakenStudents(_id: string) {
    return this.http.get(environment.url + 'student/taken/students/' + _id) ;
  }

   addStudentToTeacher(val: any){
       return this.http.put(environment.url + 'student/add/students/teacher', val );
  }

  deleteStudentTeacher(_id: string) {
    return this.http.delete(environment.url + 'student/delete/students/teacher/' + _id)
  }

  uploadImages(fileData, _id){
    const formData = new FormData();
    formData.append('imagen', fileData);
    formData.append('_id', _id);
    
    this.http.put(environment.url + 'student/upload', formData)
      .subscribe(res => {
        console.log(res);
        alert('SUCCESS !!');
      })
  }

}
