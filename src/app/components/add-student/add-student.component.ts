

import {  SubirArchivoService  } from '../subir-archivo.service';

import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ComponentsService} from '../components.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
;



@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html'
})
export class AddStudentComponent  {

  student: Student = new Student();
  fileData: File = null;

  constructor(private _component: ComponentsService,private _router: Router, private _route: ActivatedRoute, private _upload: SubirArchivoService ) {
    this._route.params.subscribe(params => {
      if(params.id){
        this._component.getStudent(params.id).subscribe( (response: Student) => this.student = response);
      }
    });
   }



uploadImage(fileInput: any){
  console.log(fileInput)
  this.fileData = <File>fileInput;
}



onSubmit(f: NgForm) {
if(f.valid) {
 

  if(this.student._id){

    this._component.updateStudent(this.student).subscribe( response => {
  
      if(response) {
         Swal.fire('Hecho', 'El alumno se actualizo con exito', 'success');
         this._router.navigate(['/student/list']);
      } else {
        Swal.fire('Atencion', 'Algo ha debido fallar', 'warning');
      }
    });
  } else {
    const student = new Student(f.value);
    this._component.addStudent(student).subscribe( (response: any) => {
    
       if(this.fileData) {
         this._upload.subirArchivo(this.fileData, response, 'student').then(()=> {
          if(response) {
              Swal.fire('Hecho', 'El profesor se agrego con exito', 'success');
              this._router.navigate(['/teacher/list']);
           } else {
             Swal.fire('Atencion', 'Algo ha debido fallar', 'warning');
           }
             });
           } else {
             if(response) {
               Swal.fire('Hecho', 'El profesor se agrego con exito', 'success');
               this._router.navigate(['/teacher/list']);
            } else {
              Swal.fire('Atencion', 'Algo ha debido fallar', 'warning');
            }
     
           }
    });
  }

} 

}
}

export interface student {
  _id?: string;
  nombre: string,
  apellidos: string,
  direccion: string,
  telefono: number,
  imagen: any
}

export class Student {
  _id: string;
  nombre: string;
  apellidos: string;
  direccion: string;
  telefono: number;
  imagen: any;

  constructor(data?: student) {
    this._id = data && data._id || null;
    this.nombre = data && data.nombre || null;
    this.apellidos = data && data.apellidos || null;
    this.direccion = data && data.direccion || null;
    this.telefono = data && data.telefono || null;
    this.imagen = data && data.imagen || null;
   
  }
}
