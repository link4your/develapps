import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class SubirArchivoService {
   URL_SERVICIOS = environment.url;
  constructor() {
  }




  subirArchivo(archivo: File, id: any, name?: string) {

    return new Promise((resolve, reject) => {

      const formData = new FormData();
      const xhr = new XMLHttpRequest();



      formData.append('imagen', archivo, archivo.name);
      xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            console.log('Imagen subida');
            resolve(xhr.response);
          } else {
            console.log(xhr.status);
            console.log('Fallo la subida');
            reject(xhr.response);
          }
        }
      };
      let url = '';
      url= `${environment.url}${name}/upload`;
      formData.append('name', 'imagen');
      formData.append('_id', id)
      xhr.open('PUT', url, true);
     
 

     xhr.withCredentials = true;
   
     xhr.send(formData);

    });


  }

}
