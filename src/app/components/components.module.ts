
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListStudentComponent } from './list-student/list-student.component';
import { AddStudentComponent } from './add-student/add-student.component';

import { AddTeacherComponent  } from './add-teacher/add-teacher.component';
import { ListTeacherComponent } from './list-teacher/list-teacher.component';
import { AppRoutingModule } from '../app-routing.module';
import {MatIconModule} from '@angular/material/icon';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {PhoneMatchingDirective} from '../directives/phone-matching.directive';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule,MatSortModule } from '@angular/material';
import { ConfigComponent } from './config/config.component';
import { CardStudentComponent } from './config/card-student.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';

@NgModule({
  declarations: [
  
    AddStudentComponent,
    ListStudentComponent,
    
    AddTeacherComponent,
    ListTeacherComponent,
    PhoneMatchingDirective,
    ConfigComponent,
    CardStudentComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    FormsModule , 
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    DragDropModule,
    MaterialFileInputModule 
  ],
  exports: [
   
    AddStudentComponent,
    ListStudentComponent,
 
    AddTeacherComponent,
    ListTeacherComponent,
    PhoneMatchingDirective,
    ConfigComponent,
    CardStudentComponent
  ],
  entryComponents: [ CardStudentComponent]
})
export class ComponentsModule { }
